"""A setuptools based setup module."""
from setuptools import setup, find_packages

setup(
    name='chess10',
    version='1.0.0',
    description='Find unique configurations of a chess board.',
    url='https://bitbucket.org/pghilardi/chess10/',
    author='Pedro Ghilardi',
    author_email='pghilardi@gmail.com',
    packages=find_packages(),
    entry_points = {
          'console_scripts': [
              'chess10=scripts.main:main',
          ],
      },
)
