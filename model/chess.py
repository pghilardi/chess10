"""Contains the chess class that knows how to find unique configurations of a
game."""

from model.board import Board


class Chess(object):
    """
    Represents a chess game and knows how to find the unique configurations
    given a set of pieces.
    """

    def __init__(self, m_rows, n_columns):
        """
        :param int m_rows:
        :param int n_columns:
        """
        self._m_rows = m_rows
        self._n_columns = n_columns


    def find_unique_representations(self, pieces):
        """
        :param list(unicode) pieces:
            The pieces to find the unique representations in a chess board.
            Ex: ['K', 'K', 'N', 'N']

        :returns list(Board):
            The unique representations boards.
        """
        from itertools import permutations
        from model.piece import PieceFactory
        from concurrent.futures import ProcessPoolExecutor, as_completed

        import multiprocessing
        cpu_count = multiprocessing.cpu_count()

        piece_factory = PieceFactory()

        # Performance: Why use futures?
        # Futures speed-up this algorithm because each permutation may be
        # solved separately. In a simple comparison when we have more than
        # ten pieces with a number of permutations, the algorithm runs two times
        # faster (at least!). The futures use all CPU's available in the machine.
        # For simple cases a minimal overhead will slow the result, but on long
        # term and to huge cases it pay offs.
        futures = []
        with ProcessPoolExecutor(max_workers=cpu_count) as executor:
            permutations = set(permutations(pieces))
            for permutation in permutations:
                board = Board(self._m_rows, self._n_columns)
                pieces = [piece_factory.create(piece_type) for piece_type in permutation]
                futures.append(executor.submit(self._solve_permutation, [], board, pieces, 0, 0, 0))

        configurations = []
        for future in as_completed(futures):
            configurations += future.result()

        return configurations

    def _solve_permutation(self, output, board, pieces, piece_index, start_x, start_y):
        """
        :param list output:
            The output where the configurations will be appended.

        :param Board board:
            The chess board.

        :param list pieces:
            The pieces to check.

        :param int piece_index:
            The current piece index.

        :param int start_x:
            The current x position.

        :param int start_y:
            The current y position.
        """

        def threats(board, piece):
            """
            :param Board board:
                The board.

            :param Piece piece:
                The piece.

            :returns bool:
                True if this piece threats with any other, False otherwise.
            """
            current_pieces = [
                position.piece for row in board.positions \
                    for position in row if position.piece is not None
            ]

            for current_piece in current_pieces:
                if current_piece.threats(piece) or piece.threats(current_piece):
                    return True

            return False

        if piece_index == len(pieces):
            # Found a valid configuration, append it to the valid ones.
            output.append(str(board))
            return output

        piece = pieces[piece_index]
        next_x = start_x
        next_y = start_y

        if not board.is_valid_position(next_x, next_y):
            # When a position is invalid, just returns
            return output

        while next_x * next_y < self._m_rows * self._n_columns:

            board.put_piece(next_x, next_y, piece)
            if not threats(board, piece):
                # We need to copy the board to recursivelly find new configs
                copied_board = board.copy()
                x_position, y_position = self._get_next_position(next_x, next_y)
                self._solve_permutation(
                    output,
                    copied_board,
                    pieces,
                    piece_index + 1,
                    x_position,
                    y_position
                )

            # Backtracking: remove the piece and try a next position
            board.remove_piece(next_x, next_y)
            next_x, next_y = self._get_next_position(next_x, next_y)

            if not board.is_valid_position(next_x, next_y):
                # When a position is invalid, just returns
                return output

        return output

    def _get_next_position(self, x_position, y_position):
        """
        :param int x_position:
            The x position on the board.

        :param int y_position:
            The y position on the board.

        :returns tuple(int, int) | tuple(None, None)
            The next position in the board or None when the next position is unreachable.
        """
        y_position = y_position + 1 if y_position + 1 < self._n_columns else 0
        x_position = x_position + 1 if y_position == 0 else x_position
        if x_position + 1 > self._m_rows:
            return None, None

        return x_position, y_position
