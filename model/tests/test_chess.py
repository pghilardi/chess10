"""Unit-tests for the chess."""
import unittest


class TestChess(unittest.TestCase):
    """
    Test the chess unique configurations.
    """

    def test_chess_case_1(self):
        """
        Test the chess unique representations.
        """
        from model.chess import Chess
        chess = Chess(3, 3)
        expected_representations = set([
            'Board: \n| ||R|| |\n| || || |\n|K|| ||K|\n',
            'Board: \n|K|| ||K|\n| || || |\n| ||R|| |\n',
            'Board: \n|K|| || |\n| || ||R|\n|K|| || |\n',
            'Board: \n| || ||K|\n|R|| || |\n| || ||K|\n'
        ])
        representations = chess.find_unique_representations('KKR')
        self.assertSetEqual(set(representations), expected_representations)

    def test_chess_case_2(self):
        from model.chess import Chess
        chess = Chess(4, 4)
        expected_representations = set([
            'Board: \n|N|| ||N|| |\n| ||R|| || |\n|N|| ||N|| |\n| || || ||R|\n',
            'Board: \n|N|| ||N|| |\n| || || ||R|\n|N|| ||N|| |\n| ||R|| || |\n',
            'Board: \n| ||N|| ||N|\n|R|| || || |\n| ||N|| ||N|\n| || ||R|| |\n',
            'Board: \n| ||N|| ||N|\n| || ||R|| |\n| ||N|| ||N|\n|R|| || || |\n',
            'Board: \n|R|| || || |\n| ||N|| ||N|\n| || ||R|| |\n| ||N|| ||N|\n',
            'Board: \n| ||R|| || |\n|N|| ||N|| |\n| || || ||R|\n|N|| ||N|| |\n',
            'Board: \n| || ||R|| |\n| ||N|| ||N|\n|R|| || || |\n| ||N|| ||N|\n',
            'Board: \n| || || ||R|\n|N|| ||N|| |\n| ||R|| || |\n|N|| ||N|| |\n'
        ])
        representations = chess.find_unique_representations('RRNNNN')
        self.assertSetEqual(set(representations), expected_representations)

    def test_chess_with_big_number_of_representations(self):
        from model.chess import Chess
        chess = Chess(6, 6)
        representations = chess.find_unique_representations('KR')
        self.assertEqual(len(representations), 800)

    def test_chess_with_huge_number_of_representations(self):
        from model.chess import Chess
        chess = Chess(10, 10)
        representations = chess.find_unique_representations('QB')
        self.assertEqual(len(representations), 6960)

    def test_chess_with_all_type_of_pieces(self):
        from model.chess import Chess
        chess = Chess(4, 4)
        representations = chess.find_unique_representations('QBRKN')
        self.assertEqual(len(representations), 16)

    def test_chess_without_valid_represetation(self):
        from model.chess import Chess
        chess = Chess(3, 3)
        representations = chess.find_unique_representations('QBRKN')
        self.assertEqual(len(representations), 0)


if __name__ == '__main__':
    unittest.main()
