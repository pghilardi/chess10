"""Unit-tests for the board."""
import unittest


class TestBoard(unittest.TestCase):
    """
    Test the board.
    """

    def setUp(self):
        '''
        Set-ups a board 5 x 5 with some pieces to be used in other tests.
        '''
        from model.board import Board
        from model.piece import King, Knight, Queen

        self.board = Board(5, 5)

        self.king_1 = King()
        self.king_2 = King()
        self.knight_1 = Knight()
        self.queen_1 = Queen()

        self.board.put_piece(2, 4, self.king_1)
        self.board.put_piece(2, 3, self.king_2)
        self.board.put_piece(0, 0, self.knight_1)
        self.board.put_piece(4, 4, self.queen_1)

    def test_board_size(self):
        """
        Tests the board size (m * n)
        """
        self.assertEqual(self.board.get_size(), 25)

        from model.board import Board
        other_board = Board(8, 8)
        self.assertEqual(other_board.get_size(), 64)

    def test_put_piece(self):
        """
        Tests the put piece operation.
        """
        from model.piece import Rook, Queen, Bishop
        bishop_1 = Bishop()
        queen_2 = Queen()
        rook_1 = Rook()

        self.board.put_piece(3, 4, rook_1)
        self.assertEqual(self.board.get_piece(3, 4), rook_1)

        self.board.put_piece(3, 3, bishop_1)
        self.assertEqual(self.board.get_piece(3, 3), bishop_1)

        self.board.put_piece(2, 2, queen_2)
        self.assertEqual(self.board.get_piece(2, 2), queen_2)

        self.assertRaises(RuntimeError, self.board.put_piece, 10, 10, self.king_1)

        self.assertRaisesRegex(
            RuntimeError,
            'The position 2, 2 already has a piece.', self.board.put_piece, 2, 2, self.king_1)

    def test_get_piece(self):
        """
        Tests the get piece operation.
        """
        self.assertEqual(self.board.get_piece(2, 4), self.king_1)
        self.assertEqual(self.board.get_piece(2, 3), self.king_2)
        self.assertEqual(self.board.get_piece(1, 1), None)
        self.assertEqual(self.board.get_piece(0, 0), self.knight_1)
        self.assertEqual(self.board.get_piece(4, 4), self.queen_1)

        self.assertRaises(RuntimeError, self.board.get_piece, 10, 10,)

    def test_has_piece(self):
        """
        Tests the has piece operation.
        """
        self.assertFalse(self.board.has_piece(1, 1))
        self.assertTrue(self.board.has_piece(2, 4))

        self.assertRaises(RuntimeError, self.board.has_piece, 10, 10,)

    def test_remove_piece(self):
        """
        Tests the remove piece operation.
        """
        self.board.remove_piece(2, 4)
        self.assertFalse(self.board.has_piece(2, 4))

        self.assertRaises(RuntimeError, self.board.remove_piece, 10, 10)

    def test_position_validation(self):
        """
        Tests the valid positions of a board.
        """
        # Check valid ones
        self.assertTrue(self.board.is_valid_position(0, 0))
        self.assertTrue(self.board.is_valid_position(1, 2))
        self.assertTrue(self.board.is_valid_position(3, 2))

        # Check boundaries
        self.assertTrue(self.board.is_valid_position(4, 4))
        self.assertFalse(self.board.is_valid_position(5, 5))
        self.assertFalse(self.board.is_valid_position(10, 0))
        self.assertFalse(self.board.is_valid_position(-1, 0))
        self.assertFalse(self.board.is_valid_position(-1, -1))

    def test_board_repr(self):
        """
        Tests the board __repr__.
        """
        from model.board import Board
        board = Board(5, 5)
        board_repr = repr(self.board)
        expected_board_repr = """Board: \n|N|| || || || |\n| || || || || |\n| || || ||K||K|\n| || || || || |\n| || || || ||Q|\n"""
        self.assertEqual(board_repr, expected_board_repr)

    def test_copy(self):
        """
        Tests that a board may be copied and it has the same __repr__, pieces
        and size.
        """
        board_repr = repr(self.board)
        copied_board = self.board.copy()

        copied_board_repr = repr(copied_board)
        self.assertEqual(board_repr, copied_board_repr)
        self.assertEqual(self.board.get_size(), copied_board.get_size())


if __name__ == '__main__':
    unittest.main()
