"""Unit-tests for the position."""
import unittest


class TestPosition(unittest.TestCase):
    """
    Test the positions.
    """

    def test_position(self):
        """
        Tests the position.
        """
        from model.position import Position
        from model.piece import King

        position = Position(1, 1)
        self.assertTrue(position.is_free())

        position.piece = King()
        self.assertFalse(position.is_free())

    def test_position_copy(self):
        """
        Tests that a position may be copied, and keeps its piece.
        """
        from model.position import Position
        from model.piece import King

        position = Position(2, 2)
        position.piece = King()

        copied_position = position.copy()
        self.assertEqual(position.piece, copied_position.piece)
        self.assertFalse(position.is_free())
        self.assertFalse(position.is_free())


if __name__ == '__main__':
    unittest.main()
