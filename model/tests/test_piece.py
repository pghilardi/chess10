"""Unit-tests for the pieces."""
import unittest


class TestPiece(unittest.TestCase):
    """
    Test the pieces.
    """

    def test_factory_valid_types(self):
        """
        Tests the valid types that can be created by the piece factory.
        """
        from model.piece import PieceFactory, PieceType
        factory = PieceFactory()

        expected_valid_types = [
            PieceType.KNIGHT.value,
            PieceType.KING.value,
            PieceType.QUEEN.value,
            PieceType.ROOK.value,
            PieceType.BISHOP.value,
        ]
        self.assertSetEqual(
            set([t for t in factory.get_valid_types()]),
            set([t for t in expected_valid_types])
        )

    def test_factory_create(self):
        """
        Tests the factory creation.
        """
        from model.piece import PieceFactory
        from model.piece import PieceType
        factory = PieceFactory()

        queen = factory.create(PieceType.QUEEN.value)
        self.assertEqual(queen.position, None)

        from model.piece import Queen
        self.assertEqual(type(queen), Queen)

        self.assertRaises(ValueError, factory.create, 'X')

    def test_king_threatening(self):
        """
        Tests which positions the king threats.
        """
        from model.board import Board
        from model.piece import King
        king_1 = King()
        king_2 = King()

        board = Board(5, 5)
        board.put_piece(2, 2, king_1)
        board.put_piece(4, 4, king_2)
        self.assertFalse(king_1.threats(king_2))

        king_3 = King()
        board.put_piece(3, 3, king_3)
        self.assertTrue(king_2.threats(king_3))

    def test_rook_threatening(self):
        """
        Tests which positions the rook threats.
        """
        from model.board import Board
        from model.piece import King, Rook, Knight
        king = King()
        king_2 = King()
        king_3 = King()
        king_4 = King()
        king_5 = King()
        rook = Rook()
        knight = Knight()

        board = Board(3, 3)
        board.put_piece(0, 0, rook)

        board.put_piece(0, 2, king)
        self.assertTrue(rook.threats(king))

        board.put_piece(0, 1, knight)
        self.assertTrue(rook.threats(knight))

        board.put_piece(1, 0, king_4)
        self.assertTrue(rook.threats(king_4))

        board.put_piece(2, 0, king_5)
        self.assertTrue(rook.threats(king_5))

        board.put_piece(1, 2, king_2)
        self.assertFalse(rook.threats(king_2))

        board.put_piece(2, 1, king_3)
        self.assertFalse(rook.threats(king_3))

    def test_bishop_threatening(self):
        """
        Tests which positions the bishops threats.
        """
        from model.board import Board
        from model.piece import Bishop, Rook, King
        bishop = Bishop()
        rook = Rook()
        king = King()
        king_2 = King()
        king_3 = King()

        board = Board(3, 3)
        board.put_piece(0, 0, rook)

        board.put_piece(2, 2, bishop)
        self.assertTrue(bishop.threats(rook))

        board.put_piece(1, 1, king)
        self.assertTrue(bishop.threats(king))

        board.put_piece(1, 2, king_2)
        self.assertFalse(bishop.threats(king_2))

        board.put_piece(2, 1, king_3)
        self.assertFalse(bishop.threats(king_3))

    def test_knight_threatening(self):
        """
        Tests which positions the knight threats.
        """
        from model.board import Board
        from model.piece import Knight, King
        knight = Knight()
        king = King()
        king_2 = King()
        king_3 = King()

        board = Board(3, 3)
        board.put_piece(0, 0, knight)

        board.put_piece(1, 2, king)
        self.assertTrue(knight.threats(king))

        board.put_piece(2, 1, king_2)
        self.assertTrue(knight.threats(king_2))

        board.put_piece(2, 2, king_3)
        self.assertFalse(knight.threats(king_3))

    def test_queen_threatening(self):
        """
        Tests which positions the queen threats.
        """
        from model.board import Board
        from model.piece import Queen, King
        queen = Queen()

        board = Board(3, 3)
        board.put_piece(0, 0, queen)

        for i in range(3):
            for j in range(3):
                if i == 0 and j == 0:
                    continue

                king = King()
                board.put_piece(i, j, king)
                if i == 2 and j == 1:
                    self.assertFalse(queen.threats(king))
                elif i == 1 and j == 2:
                    self.assertFalse(queen.threats(king))
                else:
                    self.assertTrue(queen.threats(king))

    def test_piece_repr(self):
        """
        Tests the piece __repr__.
        """
        from model.piece import Queen, King, Knight, Bishop, Rook
        queen = Queen()
        self.assertEqual(repr(queen), 'Q')
        king = King()
        self.assertEqual(repr(king), 'K')
        knight = Knight()
        self.assertEqual(repr(knight), 'N')
        bishop = Bishop()
        self.assertEqual(repr(bishop), 'B')
        Rook = Rook()
        self.assertEqual(repr(Rook), 'R')


if __name__ == '__main__':
    unittest.main()
