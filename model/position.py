"""Contains the position of a chess board."""


class Position(object):
    """
    Represents a position in a chess game. The position may be occupied or not.
    """

    def __init__(self, x_position, y_position):
        """
        :param int x_position:
        :param int y_position:
        """
        self.piece = None
        self.x_position = x_position
        self.y_position = y_position


    def copy(self):
        """
        :returns Position:
            The copied position.
        """
        position = Position(self.x_position, self.y_position)
        position.piece = self.piece
        return position


    def is_free(self):
        """
        :returns bool:
            True if is free, False otherwise.
        """
        return self.piece is None
