"""Contains the board of a chess game."""
from model.position import Position


class Board(object):
    """
    Represents a M X N board of a chess game.
    """

    def __init__(self, m_rows, n_columns):
        """
        :param m_rows:
        :param n_columns:
        """
        self._m_rows = m_rows
        self._n_columns = n_columns
        self.positions = [
            [Position(i, j) for j in range(n_columns)] for i in range(m_rows)]

    def __repr__(self):
        repr = 'Board: \n'
        for row in self.positions:
            for pos in row:
                repr += '|%s|' % str(pos.piece) if pos.piece else '| |'
            repr += '\n'
        return repr

    def copy(self):
        """
        Copy the current state of the board with the positions.
        """
        board = Board(self._m_rows, self._n_columns)

        copied_positions = []
        for row in self.positions:
            copied_row = []
            for position in row:
                copied_position = position.copy()
                copied_row.append(copied_position)

            copied_positions.append(copied_row)

        board.positions = copied_positions
        return board

    def _check_position(self, x_position, y_position):
        """
        :param int x_position:
        :param int y_position:

        :raise RuntimeError:
            When the position is invalid.
        """
        if not self.is_valid_position(x_position, y_position):
            raise RuntimeError('Cannot execute operation on a invalid position.')

    def has_piece(self, x_position, y_position):
        """
        :param int x_position:
        :param int y_position:

        :returns bool:
            True if this coordinate has a piece.
        """
        self._check_position(x_position, y_position)
        return not self.positions[x_position][y_position].is_free()

    def get_piece(self, x_position, y_position):
        """
        :param int x_position:
        :param int y_position:

        :returns Piece:
            The piece in the given coordinate.
        """
        self._check_position(x_position, y_position)
        return self.positions[x_position][y_position].piece

    def is_valid_position(self, x_position, y_position):
        """
        :param int x_position:
        :param int y_position:

        :returns bool:
            True if this coordinate is a valid position in the chess board.
        """
        if x_position is None or y_position is None:
            return False

        return 0 <= x_position < self._m_rows and 0 <= y_position < self._n_columns

    def put_piece(self, x_position, y_position, piece):
        """
        :param int x_position:
        :param int y_position:
        :param Piece piece:
        """
        if self.has_piece(x_position, y_position):
            raise RuntimeError('The position %s, %s already has a piece.' % (x_position, y_position))

        self._check_position(x_position, y_position)

        # To ease the access to x and y we associate piece and position
        position = self.positions[x_position][y_position]
        position.piece = piece
        piece.position = position

    def remove_piece(self, x_position, y_position):
        """
        :param int x_position:
        :param int y_position:

        :returns Piece:
            The removed piece.
        """
        self._check_position(x_position, y_position)
        piece = self.get_piece(x_position, y_position)
        self.positions[x_position][y_position] = Position(x_position, y_position)
        return piece

    def get_size(self):
        """
        :returns int:
            The board size.
        """
        return self._m_rows * self._n_columns
