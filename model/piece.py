"""
Contains the piece factory and the piece types of a chess game.
"""
from enum import Enum


class PieceType(Enum):
    """
    Enumeration of valid piece types.
    """

    KNIGHT = 'N'
    KING = 'K'
    QUEEN = 'Q'
    ROOK = 'R'
    BISHOP = 'B'

class PieceFactory(object):
    """
    Knows how to build chess pieces.
    """

    def __init__(self):
        self._piece_type_to_piece_class = {
            PieceType.KNIGHT.value : Knight,
            PieceType.KING.value : King,
            PieceType.QUEEN.value : Queen,
            PieceType.ROOK.value : Rook,
            PieceType.BISHOP.value : Bishop,
        }

    def get_valid_types(self):
        """
        :returns list(PieceType):
            The valid types.
        """
        return self._piece_type_to_piece_class.keys()

    def create(self, piece_type):
        """
        :param PieceType piece_type:
            The piece type.
        """
        if not piece_type in self._piece_type_to_piece_class:
            raise ValueError("Invalid piece type: %s" % piece_type)

        return self._piece_type_to_piece_class[piece_type]()


class Piece(object):
    """
    Represents a piece of a chess game.
    """

    def __init__(self):
        self.position = None

    def _can_attack(self, other_piece_x, other_piece_y):
        """
        :param int other_piece_x:
        :param int other_piece_y:

        :returns Boolean:
            True if can attack other piece, False otherwise.
        """

    def threats(self, other_piece):
        """
        :param Piece other_piece:
            The other piece to check if is threatning with this piece.

        :returns bool:
            True if threats, False otherwise.
        """
        if self == other_piece:
            return False

        other_piece_x = other_piece.position.x_position
        other_piece_y = other_piece.position.y_position
        return self._can_attack(other_piece_x, other_piece_y)


class King(Piece):
    """
    Represents a piece of a chess game.
    """

    def _can_attack(self, x, y):
        if abs(self.position.x_position - x) == 1 and abs(self.position.y_position - y) == 1:
            return True

        if self.position.x_position == x and abs(self.position.y_position - y) == 1 \
            or self.position.y_position == y and abs(self.position.x_position - x) == 1:
            return True

        return False

    def __repr__(self):
        return PieceType.KING.value


class Knight(Piece):
    """
    Represents a piece of a chess game.
    """

    def _can_attack(self, x, y):
        row_diff = abs(self.position.x_position - x)
        column_diff = abs(self.position.y_position - y)
        if row_diff == 2 and column_diff == 1 or row_diff == 1 and column_diff == 2:
            return True

        return False

    def __repr__(self):
        return PieceType.KNIGHT.value


class Bishop(Piece):
    """
    Represents a piece of a chess game.
    """

    def _can_attack(self, x, y):
        if abs(self.position.x_position - x) == abs(self.position.y_position - y):
            return True

        return False

    def __repr__(self):
        return PieceType.BISHOP.value


class Rook(Piece):
    """
    Represents a piece of a chess game.
    """

    def _can_attack(self, x, y):
        if self.position.x_position == x or self.position.y_position == y:
            return True

        return False

    def __repr__(self):
        return PieceType.ROOK.value


class Queen(Piece):
    """
    Represents a piece of a chess game.
    """

    def _can_attack(self, x, y):
        if self.position.x_position == x or self.position.y_position == y:
            return True

        if abs(self.position.x_position - x) == abs(self.position.y_position - y):
            return True

        return False

    def __repr__(self):
        return PieceType.QUEEN.value
