"""Unit-tests for the main script."""
import unittest
import sys
from io import StringIO
from contextlib import contextmanager
from unittest.mock import patch
from scripts.main import ChessArgumentParser
from scripts.main import main


@contextmanager
def capture(func, *args, **kwargs):
    """
    Executes a function, redirecting the stdout to the captured output.
    """
    out, sys.stdout = sys.stdout, StringIO()
    func(*args, **kwargs)
    sys.stdout.seek(0)
    yield sys.stdout.read()
    sys.stdout = out


class ChessArgs(object):
    """
    Mocked arguments.
    """

    def __init__(self):
        self.rows = 3
        self.columns = 3
        self.knights = 0
        self.kings = 0
        self.bishops = 0
        self.queens = 0
        self.rooks = 0
        self.max = 100


class TestMain(unittest.TestCase):
    """
    Test the main script.
    """

    def test_argument_parser_default_configuration(self):
        """
        Tests the default configuration of the argument parser.
        """
        parser = ChessArgumentParser()
        expected_description = """Find all unique configurations of a set of normal chess
        pieces on a chess board with dimensions M x N where none of the pieces is
        in a position to take any of the others.
        """
        self.assertEqual(parser.description, expected_description)

        args = parser.parse_args(['-n 1'])
        self.assertEqual(args.rows, 3)
        self.assertEqual(args.columns, 3)
        self.assertEqual(args.knights, 1)
        self.assertEqual(args.bishops, 0)
        self.assertEqual(args.queens, 0)
        self.assertEqual(args.rooks, 0)
        self.assertEqual(args.kings, 0)
        self.assertEqual(args.max, 100)

    def test_argument_parser_with_one_configuration(self):
        """
        Tests the argument parser with one valid configuration.
        """
        parser = ChessArgumentParser()
        args = parser.parse_args(['-x 5', '-y 5', '-n 1', '-b 2', '-q 3', '-r 6', '-k 1', '-m 500'])
        self.assertEqual(args.rows, 5)
        self.assertEqual(args.columns, 5)
        self.assertEqual(args.knights, 1)
        self.assertEqual(args.bishops, 2)
        self.assertEqual(args.queens, 3)
        self.assertEqual(args.rooks, 6)
        self.assertEqual(args.kings, 1)
        self.assertEqual(args.max, 500)

    def test_main_with_no_pieces(self):
        """
        Tests the main script with no pieces.
        """
        parsed_args = ChessArgs()
        with patch.object(ChessArgumentParser, 'parse_args', return_value=parsed_args) as mock_method:
            with capture(main) as output:
                expected_output = """\nErrors found:\n* No pieces were provided.\n\n\n"""
                self.assertEqual(output, expected_output)

    def test_main_with_invalid_size(self):
        """
        Tests the main script with an invalid size.
        """
        parsed_args = ChessArgs()
        parsed_args.rows = 1
        parsed_args.columns = 1
        parsed_args.knights  = 2
        with patch.object(ChessArgumentParser, 'parse_args', return_value=parsed_args) as mock_method:
            with capture(main) as output:
                expected_output = """\nErrors found:\n* The board must have at least 2 positions.\n\n\n"""
                self.assertEqual(output, expected_output)

    def test_main_with_valid_configuration(self):
        """
        Tests the main script with a valid configuration.
        """
        parsed_args = ChessArgs()
        parsed_args.rows = 5
        parsed_args.columns = 5
        parsed_args.knights  = 2
        parsed_args.kings  = 3
        parsed_args.max  = 1
        with patch.object(ChessArgumentParser, 'parse_args', return_value=parsed_args) as mock_method:
            with capture(main) as output:
                splitted = output.split('\n')

        self.assertEqual(splitted[0], "")
        self.assertEqual(splitted[1], "Number of representations found: 7836 ")
        self.assertEqual(splitted[2], "")
        self.assertEqual(splitted[3], "Printing 1 of 7836 representations: ")
        self.assertEqual(splitted[4], "")
        self.assertEqual(splitted[5], "Board: ")

if __name__ == '__main__':
    unittest.main()
