'''
Main entry to find all unique configurations of a given chess board, given a
set of pieces.
'''
# pylint: disable-msg=C0103
import sys
import argparse


class ChessArgumentParser(argparse.ArgumentParser):
    """
    The argument parser for the chess unique configurations.
    """

    def __init__(self):
        description = """Find all unique configurations of a set of normal chess
        pieces on a chess board with dimensions M x N where none of the pieces is
        in a position to take any of the others.
        """
        argparse.ArgumentParser.__init__(self, description=description)

        board_dimensions = self.add_argument_group('Dimensions')
        board_dimensions.add_argument(
            '-x', '--rows', type=int, default=3, help='Rows (default 3)')
        board_dimensions.add_argument(
            '-y', '--columns', type=int, default=3, help='Columns (default 3)')

        pieces = self.add_argument_group('Pieces')
        pieces.add_argument('-n', '--knights', type=int, default=0)
        pieces.add_argument('-b', '--bishops', type=int, default=0)
        pieces.add_argument('-q', '--queens', type=int, default=0)
        pieces.add_argument('-r', '--rooks', type=int, default=0)
        pieces.add_argument('-k', '--kings', type=int, default=0)

        pieces = self.add_argument_group('Configuration')
        pieces.add_argument(
            '-m', '--max', type=int, default=100, help='Maximum number of boards to print')


def main():
    # Hold all found errors and then present them to the user.
    errors = []

    arg_parser = ChessArgumentParser()
    args = arg_parser.parse_args()
    if args.columns < 2 and args.rows < 2:
        errors.append('The board must have at least 2 positions.')

    from itertools import chain
    from model.piece import PieceType
    pieces = list(
        chain(
            [PieceType.KNIGHT.value] * args.knights,
            [PieceType.BISHOP.value] * args.bishops,
            [PieceType.QUEEN.value] * args.queens,
            [PieceType.ROOK.value] * args.rooks,
            [PieceType.KING.value] * args.kings,
        )
    )
    if not pieces:
        errors.append('No pieces were provided.')

    if errors:
        print('\nErrors found:')
        for error in errors:
            print('* ' + error)
        print('\n')
        return

    from model.chess import Chess
    chess = Chess(args.rows, args.columns)
    unique_representations = chess.find_unique_representations(pieces)

    number_of_representations = len(unique_representations)
    print('\nNumber of representations found: %s \n' % number_of_representations)
    print('Printing %s of %s representations: \n' % (
        min(args.max, number_of_representations), number_of_representations))

    for index, representation in enumerate(unique_representations):
        if index < args.max:
            print(representation)
