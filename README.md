# README #

**The Problem**

The problem is to find all unique configurations of a set of normal chess pieces on a chess board with dimensions M×N where none of the pieces is in a position to take any of the others. Assume the colour of the piece does not matter, and that there are no pawns among the pieces.

Write a program which takes as input:

* The dimensions of the board: M, N
* The number of pieces of each type (King, Queen, Bishop, Rook and Knight) to try and place on the board.

As output, the program should list all the unique configurations to the console for which all of the pieces can be placed on the board without threatening each other.

**The Solution**

This algorithm uses a back-tracking solution to find the unique representations given a set of pieces. First the algorithm calculates the possible permutations and then solve the problem for each permutation, back-tracking if needed.

As a performance improvement, I am using a process pool executor with futures to execute each permutation on a different CPU.

**Install**

```
#!bash

python setup.py install
```

**Usage**

```
#!bash

chess10 -h

usage: chess10 [-h] [-x ROWS] [-y COLUMNS] [-n KNIGHTS] [-b BISHOPS]
               [-q QUEENS] [-r ROOKS] [-k KINGS] [-m MAX]

Find all unique configurations of a set of normal chess pieces on a chess
board with dimensions M x N where none of the pieces is in a position to take
any of the others.

optional arguments:
  -h, --help            show this help message and exit

Dimensions:
  -x ROWS, --rows ROWS  Rows (default 3)
  -y COLUMNS, --columns COLUMNS
                        Columns (default 3)

Pieces:
  -n KNIGHTS, --knights KNIGHTS
  -b BISHOPS, --bishops BISHOPS
  -q QUEENS, --queens QUEENS
  -r ROOKS, --rooks ROOKS
  -k KINGS, --kings KINGS

Configuration:
  -m MAX, --max MAX     Maximum number of boards to print
```

**Example**

Input: 4×4 board containing 2 Rooks and 4 Knights.

```
#!bash

chess10 -x 4 -y 4 -r 2 -n 4

Number of representations found: 8

Printing 8 of 8 representations:

Board:
|R|| || || |
| ||N|| ||N|
| || ||R|| |
| ||N|| ||N|

Board:
| ||R|| || |
|N|| ||N|| |
| || || ||R|
|N|| ||N|| |

Board:
| || ||R|| |
| ||N|| ||N|
|R|| || || |
| ||N|| ||N|

Board:
| || || ||R|
|N|| ||N|| |
| ||R|| || |
|N|| ||N|| |

Board:
|N|| ||N|| |
| ||R|| || |
|N|| ||N|| |
| || || ||R|

Board:
|N|| ||N|| |
| || || ||R|
|N|| ||N|| |
| ||R|| || |

Board:
| ||N|| ||N|
|R|| || || |
| ||N|| ||N|
| || ||R|| |

Board:
| ||N|| ||N|
| || ||R|| |
| ||N|| ||N|
|R|| || || |
```